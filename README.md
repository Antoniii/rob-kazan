# pip install [pandas](https://pypi.org/project/pandas/)

![](https://gitlab.com/Antoniii/rob-kazan/-/raw/main/pandas.jpg)

## Sources

* [Структурирование массивов NumPy](https://pythonru.com/biblioteki/strukturirovanie-massivov-numpy)
* [Структурированные массивы](https://pyprog.pro/structured_arrays.html)
* [ВЫБОР СТРОК В ПАНДЕ DATAFRAME НА ОСНОВЕ УСЛОВИЙ](http://espressocode.top/selecting-rows-in-pandas-dataframe-based-on-conditions/)
* [Моя шпаргалка по pandas](https://habr.com/ru/company/ruvds/blog/494720/)
* [Введение в pandas: анализ данных на Python](https://khashtamov.com/ru/pandas-introduction/)
